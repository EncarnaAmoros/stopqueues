document.addEventListener("deviceready",function(){
  navigator.splashscreen.hide();
});
//configure our module
var miApp = angular.module('miApp', [
  'ngRoute','colasControllers','colasService', 'busquedaControllers',  'missitiosControllers' ,  'busquedaService', 'establecimientoControllers', 'establecimientoService'
]).config(['$routeProvider', '$locationProvider', function($routeProvider,$locationProvider) {
  //declare our routes
  $routeProvider.
  when('/world', {
    templateUrl: 'partials/world.html'
  }).
  when('/colas',{
      templateUrl:'partials/colas.html',
      controller: 'ColasCtrl'
  }).
  when('/busqueda',{
      templateUrl:'partials/busqueda.html',
      controller: 'BusquedaCtrl'
  }).
  when('/missitios',{
      templateUrl:'partials/missitios.html',
      controller: 'MissitiosCtrl'
  }).
  when('/establecimientos/:id',{
      templateUrl:'partials/establecimiento.html',
      controller: 'EstablecimientoCtrl'
  }).
  otherwise({
    redirectTo: '/colas'
  });
}]);
