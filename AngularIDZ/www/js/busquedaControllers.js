var busquedaControllers = angular.module('busquedaControllers', ['busquedaService']);

////////////////////////////////////////
//CONTROLADORES Y FUNCIONES DE ARTICULOS
////////////////////////////////////////

/* Mostramos los artículos de forma general */

busquedaControllers.controller('BusquedaCtrl',  ['$scope', '$http',  '$routeParams', 'mibusquedaService',
  function ($scope, $http, $routeParams, mibusquedaService) {
    //Obtenemos todas las colas para obtener luego sus establecimientos
    var verDatos = function () {
      mibusquedaService.getColas()
      .success(function(colas) {

        //Recorremos cada cola y de cada vemos el establecimiento asociado
        colas.forEach(function(cola) {

          //Tiempo promedio
          var tiempo = cola.tiempoMedio * cola.numPersonas;
          var horas = String(tiempo/60).split('.')[0]
          var minutos = tiempo%60
          cola.tiempopromedio = horas+"h "+minutos+"min"

          //Según una cola obtenemos el establecimiento
          mibusquedaService.getSitioCola(cola.id)
            .success(function(resultado) {

              //Nombre establecimiento
              cola.nombre = resultado.nombre;

              //Dada una cola miramos si el establecimiento asociado está en mis sitios
              cola.estaenmissitios = false;
              mibusquedaService.getSitiosUsuario()
                .success(function(sitiosusuario) {
                  sitiosusuario.forEach(function(sitiousuario) {
                    //id establecimiento igual a resultados2.id
                    if(resultado.id == sitiousuario.id) {
                      cola.estaenmissitios = true;
                    }
                  })        
              })
            })

        });
        
        //Resultados finales
        $scope.colas = colas;
      })
      .error(function(resultados) {
        //Sin datos
      })
    }
    verDatos();

    //Funcion para añadir un sitio como favorito
    $scope.putFavoritoSitio = function(id) {
      mibusquedaService.putFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verDatos();
      })
    }

    //Funcion para quitar un sitio como favorito
    $scope.removeFavoritoSitio = function(id) {
      mibusquedaService.removeFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verDatos();
      })
    }

  }]);
