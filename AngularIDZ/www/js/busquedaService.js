var busquedaService = angular.module('busquedaService', ['ngRoute']);
var URL_API = 'http://stop-queues.herokuapp.com/api/';

var idusuario = 1;

////////////////////////////////
//LLAMADAS AL API PARA BUSQUEDA
////////////////////////////////

busquedaService.service('mibusquedaService', function($http)
{
	return {
		getSitios: function(page) {
			return $http.get(URL_API + 'sitios');
		},
		getColas: function() {
			return $http.get(URL_API + 'colas');
		},
		getSitioCola: function(colaid) {
			return $http.get(URL_API + 'colas/' + colaid + '/sitio');
		},
		getSitiosUsuario: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/sitios');
		},
		putFavoritoSitio: function(id) {
			return $http.put(URL_API + 'users/'+idusuario+'/sitios/' + id);
		},
		removeFavoritoSitio: function(id) {
			return $http.delete(URL_API + 'users/'+idusuario+'/sitios/' + id);
		}
	}
});
