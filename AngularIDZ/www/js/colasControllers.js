var colasControllers = angular.module('colasControllers', ['colasService']);

////////////////////////////////////////
//CONTROLADORES Y FUNCIONES DE ARTICULOS
////////////////////////////////////////

/* Mostramos los artículos de forma general */

colasControllers.controller('ColasCtrl',  ['$scope', '$http',  '$routeParams', 'misColasService',
  function ($scope, $http, $routeParams, misColasService) {

    //Obtenemos los artículos llamando al service y lo mostramos en vista
    var verDatos = function() {
      misColasService.getTicket()
        .success(function (res) {

          res.forEach(function(re) {
            //Los sitios del usuario            
            re.favorito = false

            //Tiempo promedio
            var tiempo = re.tiempoMedio * re.delante;
            var horas = String(tiempo/60).split('.')[0]
            var minutos = tiempo%60
            re.tiempoMedio = horas+"h "+minutos+"min"


            misColasService.getSitiosUsuario(re.EstablecimientoId)
              .success(function(sitios) {

                //FOR EACH SITIO
                sitios.forEach(function(sitio) {
                  //Es favorito
                  console.log("sitio:"+sitio.id+", "+"estable:"+re.EstablecimientoId)
                  if(sitio.id==re.EstablecimientoId) {
                    re.favorito = true
                    console.log("ES FAV")
                  }
                })
                //console.log("miralo:"+$scope.datos.tiempoMedio)
                
              })
          })

          $scope.datos = res;
        })
    } 
    verDatos();


    //Funcion para añadir un sitio como favorito
    $scope.putFavoritoSitio = function(id) {
      misColasService.putFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verDatos();
      })
    }

    //Funcion para quitar un sitio como favorito
    $scope.removeFavoritoSitio = function(id) {
      misColasService.removeFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verDatos();
      })
    }

  }]);
