var colasService = angular.module('colasService', ['ngRoute']);
var URL_API = 'http://stop-queues.herokuapp.com/api/';

var idusuario = 1;

////////////////////////////////
//LLAMADAS AL API PARA COLAS
////////////////////////////////

colasService.service('misColasService', function($http)
{
	return {
		getColas: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/colas');
		},
		getSitioCola: function(colaid) {
			return $http.get(URL_API + 'colas/' + colaid + '/sitio');
		},
		getSitiosUsuario: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/sitios');
		},
		getTicket: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/tiquets');
		},
		putFavoritoSitio: function(id) {
			return $http.put(URL_API + 'users/'+idusuario+'/sitios/' + id);
		},
		removeFavoritoSitio: function(id) {
			return $http.delete(URL_API + 'users/'+idusuario+'/sitios/' + id);
		}
	}
});
