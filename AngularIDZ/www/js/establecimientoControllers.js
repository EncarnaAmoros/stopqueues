var establecimientoControllers = angular.module('establecimientoControllers', ['establecimientoService']);


establecimientoControllers.controller('EstablecimientoCtrl',  ['$scope', '$http',  '$routeParams', 'miEstablecimientoService',
  function ($scope, $http, $routeParams,miEstablecimientoService) {

    var verDatos = function() {
      //Obtenemos el establecimiento
      miEstablecimientoService.getSitio($routeParams.id)
        .success(function(resultado) {
          $scope.sitio = resultado;

          //Del establecimiento obtenemos la cola
          miEstablecimientoService.getCola(resultado.id)
          .success(function(cola) {

            //De la cola obtenemos datos humanos
            $scope.sitio.numPersonas = cola.numPersonas;
            $scope.cola = cola.id;
            
            //Llamada a servicio
            miEstablecimientoService.getSitio($scope.sitio.id)

            //Tiempo promedio
            var tiempo = cola.tiempoMedio * cola.numPersonas;
            var horas = String(tiempo/60).split('.')[0]
            var minutos = tiempo%60
            $scope.tiempomedio = horas+"h "+minutos+"min"
            
            //Según si está bloqueado o no mostramos unos botones u otros
            if($scope.sitio.bloqueado == false)
              $scope.colabloqueada = cola.bloqueada
            else
              $scope.colabloqueada = true

            //Favorito a sitios
            var test = false;

            //Miramos las colas del usuario
            miEstablecimientoService.getColasUsuario()
              .success(function(colasusuario) {
                //Por cada cola miramos si está en la cola del usuario
                colasusuario.forEach(function(colausuario) {   
                  console.log($scope.cola+" "+colausuario.id)             
                  if($scope.cola == colausuario.id) {
                    test = true;
                  }
                }) 
                $scope.noestaencola = test;
            })
          })

          //Miramos tickets de usuario si establecimientotiquet coincide con el establecimiento
          $scope.tengocodigo = false;
          miEstablecimientoService.getTickets()
            .success(function(tickets) {

              //Recorremos todos los tickets buscando
              tickets.forEach(function(ticket) {
                if(ticket.EstablecimientoId == $scope.sitio.id) {
                  $scope.tengocodigo = true;
                  $scope.codigo = ticket.tiquet;
                }
              })
            })
      })
    }
    verDatos();

    //Vemos si el sitio está en mis sitios
    var verSiEstaEnSitios = function() {
      $scope.estaenmissitios = false;

      //Obtenemos los sitios del usuario
      miEstablecimientoService.getSitiosUsuario()
        .success(function(sitiosusuario) {
          sitiosusuario.forEach(function(sitiousuario) {
            
            //Por cada sitio del usuario miramos si coincide con el establecimiento actual
            if($routeParams.id == sitiousuario.id) {
              $scope.estaenmissitios = true;
            }
          })       
      })
    }
    verSiEstaEnSitios();

    //Funcion para añadir a cola
    $scope.putEnCola = function(id) {
      console.log("hacemos un put cola")
      miEstablecimientoService.putEnCola(id)
        .success(function(data, status, headers, config) {
          verDatos();
        })
    }

    //Funcion para quitar a cola
    $scope.removeEnCola = function(id) {
      console.log("hacemos un remove cola")
      miEstablecimientoService.removeEnCola(id)
        .success(function(data, status, headers, config) {
          verDatos();
        })
    }

    //Funcion para añadir un sitio como favorito
    $scope.putFavoritoSitio = function(id) {
      miEstablecimientoService.putFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verSiEstaEnSitios();
      })
    }

    //Funcion para quitar un sitio como favorito
    $scope.removeFavoritoSitio = function(id) {
      miEstablecimientoService.removeFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verSiEstaEnSitios();
      })
    }

  }]);
