var establecimientoService = angular.module('establecimientoService', ['ngRoute']);
var URL_API = 'http://stop-queues.herokuapp.com/api/';

var idusuario = 1;

////////////////////////////////
//LLAMADAS AL API PARA SITIOS
////////////////////////////////

establecimientoService.service('miEstablecimientoService', function($http)
{
    return {
        getCola: function(colaid) {
            return $http.get(URL_API + 'colas/' + colaid);
        },
        getSitio: function(id) {
            return $http.get(URL_API + 'sitios/' + id);
        },
        getSitioCola: function(colaid) {
            return $http.get(URL_API + 'colas/' + colaid + '/sitio');
        },
		getSitiosUsuario: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/sitios');
		},
		getColasUsuario: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/colas');
		},
		getTickets: function() {
			return $http.get(URL_API + 'users/'+idusuario+'/tiquets');
		},
		putEnCola: function(id) {
			return $http.put(URL_API + 'users/'+idusuario+'/colas/' + id);
		},
		removeEnCola: function(id) {
			return $http({
				method: "DELETE",
				url: URL_API + 'users/'+idusuario+'/colas/' + id
			})
		},
		putFavoritoSitio: function(id) {
			return $http.put(URL_API + 'users/'+idusuario+'/sitios/' + id);
		},
		removeFavoritoSitio: function(id) {
			return $http.delete(URL_API + 'users/'+idusuario+'/sitios/' + id);
		}
    }
});
