var missitiosControllers = angular.module('missitiosControllers', ['missitiosService']);

////////////////////////////////////////
//CONTROLADORES Y FUNCIONES DE ARTICULOS
////////////////////////////////////////

/* Mostramos los artículos de forma general */

missitiosControllers.controller('MissitiosCtrl',  ['$scope', '$http',  '$routeParams', 'mimissitiosService',
  function ($scope, $http, $routeParams, mimissitiosService) {
    //Obtenemos las colas y establecimientos llamando al service y lo mostramos en vista
    var verDatos = function () {
      mimissitiosService.getSitiosUsuario()
      .success(function(sitios) {
        sitios.forEach(function(sitio) {
            
            mimissitiosService.getSitioCola(sitio.id)
            .success(function(resultado) {
              sitio.nombre = resultado.nombre;

              mimissitiosService.getColaSitio(sitio.id)
              .success(function(rescola) {
                var tiempo = rescola.tiempoMedio * rescola.numPersonas;
                var horas = String(tiempo/60).split('.')[0]
                var minutos = tiempo%60
                sitio.tiempopromedio = horas+"h "+minutos+"min"
                sitio.numPersonas = rescola.numPersonas
              })

            })
        });
        
        $scope.colas = sitios;
      }) 
      .error(function(resultados) {
        //Sin datos
      })
    }
    verDatos();

    //Funcion para quitar un sitio como favorito
    $scope.removeFavoritoSitio = function(id) {
      mimissitiosService.removeFavoritoSitio(id)
        .success(function(data, status, headers, config) {
          verDatos();
      })
    }

  }]);
