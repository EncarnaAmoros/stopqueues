var missitiosService = angular.module('missitiosService', ['ngRoute']);
var URL_API = 'http://stop-queues.herokuapp.com/api/';

var idusuario = 1;

/////////////////////////////////
//LLAMADAS AL API PARA MIS SITIOS
/////////////////////////////////

missitiosService.service('mimissitiosService', function($http)
{
	return {
		getSitiosUsuario: function() {
			return $http.get(URL_API + 'users/' + idusuario + '/sitios');
		},
		getSitioCola: function(colaid) {
			return $http.get(URL_API + 'colas/' + colaid + '/sitio');
		},
		getColaSitio: function(id) {
			return $http.get(URL_API + 'sitios/' + id + '/cola');
		},
		removeFavoritoSitio: function(id) {
			return $http.delete(URL_API + 'users/'+idusuario+'/sitios/' + id);
		}
	}
});
