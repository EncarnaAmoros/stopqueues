# STOP QUEUES

Proyecto desarrollado en la Hackathon de 2016 en Alicante. Gestión de colas de los establecimientos que contratarían el servicio, y demás tratamiento de los datos recabados.

## Equipo: 

- Encarna Amorós Beneite
- Héctor José Compañ Gabucio
- Jorge Azorín Martí
- Manuel José Verdú Ramón
- Victor García Gómez

## Licencia

Licencia creative commons

## Estructura de archivos

### Ficheros principales:

#### AngularIDZ

- Donde se encuentra el proyecto de la app móvil (clientes) Nota: los archivos centrales de la app se encuentran en AngularIDZ/www
- AngularIDZ/index.html archivo html principal donde se cargará cada html parcial (los demás html de esta ruta no se utilizan, solo como plantillas)
- AngularIDZ/www/partials: htmls de la app móvil
- AngularIDZ/www/css: estilos
- AngularIDZ/www/js: librerías javascript, controladores y servicios de AngularJS
- AngularIDZ/www/lib: librerías
- AngularIDZ/www/img: imágenes
- AngularIDZ/www/fonts: iconos (icons de bootstrap)

#### paginaweb

- Donde se encuentra el proyecto de la web para el sitio (sitios, locales, establecimientos...)



#### plantillasFinalesHTMLCSSMovil

- Donde se encuentran las plantillas HTML+CSS+Bootstrap finales de la interfaz para el desarrollo de la app móvil

### Ficheros antiguos:

#### appframework-angularjs-controller 

- Empezamos con este para la app movil pero cambiamos la forma de iniciar el proyecto

#### prototiposHTMLCSSMovil

- Prototipos iniciales para la interfaz de la app móvil